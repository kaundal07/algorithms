#include<iostream>
#include<vector>
#include<list>

using namespace std;

class Graph{
    int V;
    list<int> *adj;

    public:
        Graph(int v);
        void addEdge(int e,int v);
        void BFS(int s);
};

Graph::Graph(int v){
    this->V = v;
    adj = new list<int>[V];
}

void Graph::addEdge(int e,int v){
    adj[e].push_back(v);
}

void Graph::BFS(int s){
    vector<bool> visit(V,false);
    list<int> queue;

    queue.push_back(s);
    visit[s] = true;

    list<int>::iterator it;
    while(!queue.empty()){
        s = queue.front();
        cout<<s<<" ";
        queue.pop_front();
        for(it = adj[s].begin();it!=adj[s].end();++it){
            if(!visit[*it]){
                visit[*it] = true;
                queue.push_back(*it);
            }
        }
    }
}

using namespace std;
int main(){
    Graph G(5);
    G.addEdge(0, 1); 
    G.addEdge(0, 2); 
    G.addEdge(1, 2); 
    G.addEdge(2, 0); 
    G.addEdge(2, 3); 
    G.addEdge(3, 3); 

    G.BFS(2); 
}